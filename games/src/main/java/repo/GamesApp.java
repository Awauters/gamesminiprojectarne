package repo;

import application.Game;
import domain.Category;
import repo.CategoryDAO;
import services.CategoryService;

import java.util.List;

public class GamesApp {

    public static void main(String[] args) {
        CategoryDAO dao = new CategoryDAO();
        List<Category> categories = dao.findCategoriesWhereNameStartsWith("co");
        categories.forEach(c-> System.out.println(c));
//        Category category = dao.findCategoryById(2);
        CategoryDAO categoryDAO = new CategoryDAO();
        CategoryService categoryService = new CategoryService(categoryDAO);
        Game game = new Game(categoryService);
        game.startGame();

    }
}