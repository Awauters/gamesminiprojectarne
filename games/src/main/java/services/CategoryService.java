package services;

import domain.Category;
import exceptions.NoRecordFoundException;
import repo.CategoryDAO;

public class CategoryService {

    private CategoryDAO categoryDAO;

    public CategoryService(CategoryDAO categoryDAO){
        this.categoryDAO = categoryDAO;
    }
    public Category findCategoryById(int id) throws NoRecordFoundException {
        Category category = (Category) categoryDAO.findAllCategoryById(id);
        if (category==null){
            throw new NoRecordFoundException("Ooops, couldn't found a record with this "+id+" in our categories. Try again");
        }
        return category;
    }
}
