
package application;

import domain.Category;
import exceptions.NoRecordFoundException;
import services.CategoryService;

import java.util.HashMap;
import java.util.InputMismatchException;
import java.util.Map;
import java.util.Scanner;

public class Game {

    private Map<Integer, String> menuMap;
    private CategoryService categoryService;
    private Scanner scanner;

    public Game(CategoryService categoryService) {
        this.scanner = new Scanner(System.in);
        this.categoryService = categoryService;
        this.menuMap = new HashMap<>();
        menuMap.put(1, "Show category for id?");
    }

    public void startGame() {
        System.out.println("-------------HELLO STEVEN--------------");
        System.out.println("Game options");
        showMenu();
    }

    private void showMenu() {
        for (Map.Entry<Integer, String> m : menuMap.entrySet()) {
            System.out.println(m.getKey() + " " + m.getValue());
        }
        chooseOption();
    }

    private void chooseOption() {
        if (scanner == null) {
            scanner = new Scanner(System.in);
        }
        try {
            System.out.print("Choose your option : ");
            int i = scanner.nextInt();
            executeOption(i);
        } catch (InputMismatchException e) {
            System.err.println("I asked for a f*ing number!!!");
            scanner = null;
            chooseOption();
        }
    }

    private void executeOption(int option) {
        switch (option) {
            case 1: findCategory(option);
                break;
        }

    }

    private void findCategory(int option) {
        try {
            Category category = categoryService.findCategoryById(option);
            System.out.println(category);
        } catch (NoRecordFoundException e) {
            e.printUserFriendlyMessage();
            startGame();
        }
    }


}
