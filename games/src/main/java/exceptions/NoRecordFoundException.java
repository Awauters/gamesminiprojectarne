package exceptions;


public class NoRecordFoundException extends Exception {

    public NoRecordFoundException(String message) {
        super(message);
    }

    public void printUserFriendlyMessage() {
        System.err.println(getMessage());
    }
}
